no_proxy="127.0.0.1,localhost,localnets,.lan,.local,.home,/var/run/docker.sock"
NETWORK_SETTINGS_SCRIPT_LOCATION="https://gitlab.com/shell-bootstrap-scripts/network-settings/-/raw/master/set_variables_in_CI.sh"
COOKIECUTTER_IMAGE="pythonpackagesalpine/cookiecutter-alpine:cookiepatcher-alpine"
PYPI_URL="https://pypi.org/"
DNS_IPs_JSON_list='["8.8.8.8"]'
DOMAINS_TO_WHITELIST_FOR_SSL_SPACE_SEPARATED="gitlab.com github.com"
